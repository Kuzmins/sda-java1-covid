# Spring basics.

We will create application for management corona sda.spring.covid.patients, doctors and tests using spring.

### App requirements:

Patient module:
* Register sda.spring.covid.patients (name, surname, personalId, email).
  
* Update patient name, surname, email, personalId.
* Ask doctor for corona test. (Update status waiting for test)
* List all sda.spring.covid.patients.

Additional:
* Pagination and sorting in list.
* Search patient by name, surname, personal id, email.

Business validation for patient module:
* Do not allow register with duplicate emails.
* Do not allow  register sda.spring.covid.patients from black list (check personalId).

Doctor module:
* Register a doctor. (name, surname, clinic name, personalId, licenceId)
* Assign doctor to sda.spring.covid.patients.
* Accept patient request.
* Auto assign patient to doctors with less active sda.spring.covid.patients.

Additional:
* Pagination and sorting in list.
* Search doctor by name, surname, doctor id.

Business validation for doctor module:
* Do not allow to add doctor with same licenceId
* Do not allow accepting patient requests to corona more than x times per month (configurable).
* Do not allow assigning doctor to patient if doctor has more than x sda.spring.covid.patients (configurable).
* (Optional) Before registering a doctor check if its doctor_id exist in doctor registers (possible integration https://www.neslimo.lv/arstniecibas-personas/?search_value={doctor_id} ).

Corona test module:
* Send patient to test
* List all tests (with filtering by status).
* Update test results and data.
* Notify sda.spring.covid.patients and doctors about results.

Business validation for corona test module:
* Do not allow send patient for testing if patient test already in progress.
* Do not allow for repeating tests if previous test was made less than x days ago (should be configurable)

Black list module:
* Add clients personalId in black list with description.
* Add doctors id in black list with description.
* List all blacklisted persons
* Search blacklisted persons by a personal id or doctor id.

Business Validation:
* Do not allow duplicates personal id
* Do not allow duplicates doctor id

Web module:

Patient api:
* /api/sda.spring.covid.patients - GET (list all), POST(register)
* /api/sda.spring.covid.patients/{id} - GET (patient status), PUT(Update patient data (email))
* /api/sda.spring.covid.patients/{id}/test - GET (test status), POST(request for test)

Additional task:
* Add pagination and sorting.
* Add search filters.

Doctor api:
* /api/doctors - GET (list all), POST(register)
* /api/doctors/{id} - GET(view data), PUT (update data) 
* /api/doctors/{id}/sda.spring.covid.patients - GET(view all patiens assigned to doctor), POST(assigne new patien)
* /api/doctors/{id}/sda.spring.covid.patients/{patient_id}/ - GET(view patient data), PUT(update patient data)
* /api/doctors/{id}/sda.spring.covid.patients/{patient_id}/test - GET(view patient test requests), POST(send patient to test)
* /api/doctors/{id}/sda.spring.covid.patients/{patient_id}/test/{test_id} - GET(view test status)

Additional task:
* Add pagination and sorting.
* Add search filters.

Corona test api:
* /api/tests GET (list all tests), POST (register new test) - input data (patient_id, doctor_id)
* /api/tests/{id} GET (view test result), PUT (update test result)

Black list api:
* /api/black-list/ - GET (list all), POST(add new entry)
* /api/black-list/{id} - PUT (update entry)
* /api/black-list/{id} - DELETE (delete entry) 

Feedback module:
* /feedback - Page with feedback form.

Security module:
* Register user with roles: ROLE_PATIENT, ROLE_DOCTOR, ROLE_LAB_WORKER, ADMIN
* Add security through filters on endpoints, so that users can only view and edit own data.
* Update registration endpoints to supply password and register users.

Guidelines: 
Structure:
1. A business module
	Each module consist of:
	* Domain objects.
	* Service query service or business logic service.
	* Business validators.
	* Repository.

2. Web module:
	Contains all endpoints and have following sturcture.
	* Request dto.
	* Response dto.
	* Rest controllers.
	* Some static pages.

3. Security module:
	Contains needed classes to implement security
	* User entity and repositories
	* Security configuration

Web Controller responsibility:
1. Validate request input parameters(if needed)
2. Map input to business objects needed for services (if needed)
3. Call necessary services
4. Map result from services to response (if needed)
5. Return response or throw exception

Service responsibility:
1. Perform mapping to other objects (if needed)
2. Perform business validation of input (if needed)
3. Fetch necessary busniess data from repositories (if needed)
4. Perform business logic or call other services to perform this logic.
5. Map result(if needed) to output objects
6. return result or throw exception

Repository responsibility:
1. Validate inputs (if needed)
2. Perform necessary db operations (read, create, update, delete)
3. perform mapping (if needed)
4. return result or throw exception