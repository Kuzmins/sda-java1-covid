package sda.spring.covid.doctor;

import org.springframework.data.jpa.repository.JpaRepository;
import sda.spring.covid.patients.Patient;

import java.util.List;
import java.util.Optional;

public interface DoctorRepository extends JpaRepository<Doctor, Long> {

    Optional<Doctor> findByLicenceId(String licenceId);
    Optional<Doctor> findByPersonalId(String personalId);

    List<Doctor> findByFirstName(String firstName);
}
