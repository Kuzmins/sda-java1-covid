package sda.spring.covid.doctor;

import org.springframework.stereotype.Component;
import sda.spring.covid.validation.ValidationError;
import sda.spring.covid.validation.Validator;

import java.util.ArrayList;
import java.util.List;

@Component
public class UniqueLicenseIdDoctorValidator implements Validator<Doctor> {

    private final DoctorRepository doctorRepository;

    public UniqueLicenseIdDoctorValidator(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    @Override
    public List<ValidationError> validate(Doctor doctor) {
        List<ValidationError> errors = new ArrayList<>();
        doctorRepository.findByLicenceId(doctor.getLicenceId()).ifPresent(it -> {
            errors.add(new ValidationError("licenseNotUnique", "Doctor with that license id exist"));
        });

        return errors;
    }
}
