package sda.spring.covid.doctor;


import javax.persistence.*;
import javax.validation.constraints.*;

/*
* Make doctor an entity with id, firstName, lastName, personalId, clinicName and licenceId
* */
@Entity
@Table(name = "doctors")
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 2, max = 30)
    @Column(name = "first_name")
    private String firstName;

    @Size(min = 2, max = 30)
    @Column(name = "last_name")
    private String lastName;

    @Pattern(regexp = "^(\\d{6})-[012]\\d{4}$", message = "Not valid personalId!")
    @Column(name = "personal_Id")
    private String personalId;

    @Size(min = 2, max = 30)
    @Column(name = "clinic_name")
    private String clinicName;

    @Size(min = 2, max = 30)
    @Column(name = "licence_Id")
    private String licenceId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getClinicName() {
        return clinicName;
    }

    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }

    public String getLicenceId() {
        return licenceId;
    }

    public void setLicenceId(String licenceId) {
        this.licenceId = licenceId;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", personalId='" + personalId + '\'' +
                ", clinicName='" + clinicName + '\'' +
                ", licenceId=" + licenceId +
                '}';
    }
}
