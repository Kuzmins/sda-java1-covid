package sda.spring.covid.doctor;

import org.springframework.stereotype.Component;
import sda.spring.covid.validation.ValidationError;
import sda.spring.covid.validation.Validator;

import java.util.ArrayList;
import java.util.List;

@Component
public class UniquePersonalIdDoctorValidator implements Validator<Doctor> {

    private final DoctorRepository doctorRepository;

    public UniquePersonalIdDoctorValidator(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    @Override
    public List<ValidationError> validate(Doctor doctor) {
        List<ValidationError> errors = new ArrayList<>();

        doctorRepository.findByPersonalId(doctor.getPersonalId()).ifPresent(it -> {
            errors.add(new ValidationError("personalIdNotUnique", "Doctor with personal id exist"));
        });

        return errors;
    }
}
