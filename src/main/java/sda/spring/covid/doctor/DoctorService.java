package sda.spring.covid.doctor;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import sda.spring.covid.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class DoctorService {

    private final DoctorRepository doctorRepository;

    private final UniqueLicenseIdDoctorValidator licenseIdDoctorValidator;
    private final UniquePersonalIdDoctorValidator personalIdDoctorValidator;

    public DoctorService(DoctorRepository doctorRepository, UniqueLicenseIdDoctorValidator licenseIdDoctorValidator,
                         UniquePersonalIdDoctorValidator personalIdDoctorValidator) {
        this.doctorRepository = doctorRepository;
        this.licenseIdDoctorValidator = licenseIdDoctorValidator;
        this.personalIdDoctorValidator = personalIdDoctorValidator;
    }

    @Transactional(readOnly = true)
    public List<Doctor> listAllDoctors() {
        return doctorRepository.findAll();
    }

    @Transactional
    public void createNewDoctor(Doctor doctor) {
        List<ValidationError> errors = licenseIdDoctorValidator.validate(doctor);
        errors.addAll(personalIdDoctorValidator.validate(doctor));

        if (!errors.isEmpty()) {
            throw new DoctorException(errors);
        }

        doctorRepository.save(doctor);
    }

    @Transactional
    public void updateDoctor(Doctor doctor) {
        List<ValidationError> errors = checkDoctorBeforeUpdate(doctor);

        if (!errors.isEmpty()) {
            throw new DoctorException(errors);
        }

        doctorRepository.save(doctor);
    }

    private List<ValidationError> checkDoctorBeforeUpdate(Doctor doctor) {
        Optional<Doctor> doctorInDb = doctorRepository.findById(doctor.getId());

        List<ValidationError> errors = new ArrayList<>();

        if (doctorInDb.isPresent()) {
            Doctor doctorToCheck = doctorInDb.get();
            if (!doctorToCheck.getPersonalId().equals(doctor.getPersonalId()) ) {
                errors.addAll(personalIdDoctorValidator.validate(doctor));
            }
            if (!doctorToCheck.getLicenceId().equals(doctor.getLicenceId()) ) {
                errors.addAll(licenseIdDoctorValidator.validate(doctor));
            }
        }

        return errors;
    }

    public void deleteDoctor(Long doctorId) {
        doctorRepository.deleteById(doctorId);
    }

    public Optional<Doctor> findById(Long doctorId) {
        return doctorRepository.findById(doctorId);
    }
}
