package sda.spring.covid.doctor;

import sda.spring.covid.validation.ValidationError;

import java.util.List;

public class DoctorException extends RuntimeException {
    private final List<ValidationError> errors;

    public DoctorException(List<ValidationError> errors) {
        this.errors = errors;
    }

    public List<ValidationError> getErrors() {
        return errors;
    }
}
