package sda.spring.covid.validation;

import java.util.List;

public interface Validator<T> {
    List<ValidationError> validate(T object);
}
