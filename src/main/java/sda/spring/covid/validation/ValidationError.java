package sda.spring.covid.validation;

public class ValidationError {
    private String code;
    private String errorMessage;

    public ValidationError(String code, String errorMessage) {
        this.code = code;
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
