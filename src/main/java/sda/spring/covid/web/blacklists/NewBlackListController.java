package sda.spring.covid.web.blacklists;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import sda.spring.covid.blacklist.BlackList;
import sda.spring.covid.blacklist.BlackListService;

import javax.validation.Valid;

@Controller
public class NewBlackListController {
    private BlackListService blackListService;

    public NewBlackListController(BlackListService blackListService) {
        this.blackListService = blackListService;
    }

    @GetMapping("/blacklist/new")
    public String showAddBlackListForm(Model model) {
        model.addAttribute("blackList", new BlackList());
        return "blacklist-new";
    }

    @PostMapping("/blacklist/new")
    public String addNewBlackList(@Valid @ModelAttribute BlackList blackList, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "blacklist-new";
        }

        if (blackListService.findByPersonalId(blackList.getPersonalId()).isPresent()) {
            model.addAttribute("errorMessage", "Blacklist with that id exist");
            return "blacklist-new";
        }

        blackListService.saveBlackList(blackList);
        return "redirect:/blacklist";
    }
}
