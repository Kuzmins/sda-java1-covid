package sda.spring.covid.web.blacklists;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import sda.spring.covid.blacklist.BlackList;
import sda.spring.covid.blacklist.BlackListService;

import java.util.List;

@Controller
public class BlackListViewController {

    private final BlackListService blackListService;

    public BlackListViewController(BlackListService blackListService) {
        this.blackListService = blackListService;
    }

    @GetMapping("/blacklist")
    public String showAllDoctors(Model model) {
        List<BlackList> allInBlackList = blackListService.listAllInBlackList();
        model.addAttribute("blacklist", allInBlackList);
        return "blacklist";
    }

    @DeleteMapping("/blacklist/{id}")
    public String deleteFromBlackList(@PathVariable("id") Long blackListId) {
        blackListService.deleteFromBlackList(blackListId);
        return "redirect:/blacklist";
    }
}
