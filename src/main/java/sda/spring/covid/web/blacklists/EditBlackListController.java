package sda.spring.covid.web.blacklists;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sda.spring.covid.blacklist.BlackList;
import sda.spring.covid.blacklist.BlackListService;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class EditBlackListController {

    private final BlackListService blackListService;

    public EditBlackListController(BlackListService blackListService) {
        this.blackListService = blackListService;
    }

    @GetMapping("/blacklist/edit/{id}")
    public String getEditBlackListForm(@PathVariable("id") Long blackListId, Model model, RedirectAttributes redirectAttrs) {
        Optional<BlackList> blackListToEdit = blackListService.findById(blackListId);

        if (blackListToEdit.isPresent()) {
            model.addAttribute("blacklist", blackListToEdit.get());
            return "blacklist-edit";
        } else {
            redirectAttrs.addFlashAttribute("errorMessage", "BlackList not found");
            return "redirect:/blacklist";
        }
    }

    @PostMapping("/blacklist/edit/{id}")
    public String updateDoctor(@Valid @ModelAttribute("blacklist") BlackList blackList, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "blacklist-edit";
        }
        blackListService.saveBlackList(blackList);
        return "redirect:/blacklist";
    }
}
