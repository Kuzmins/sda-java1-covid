package sda.spring.covid.web.patients;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import sda.spring.covid.patients.Patient;
import sda.spring.covid.patients.PatientService;

import java.util.List;

@Controller
public class PatientViewController {

    private final PatientService patientService;

    public PatientViewController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping("/patients")
    public String showAllPatients(Model model) {
        List<Patient> allPatients = patientService.listAllPatients();
        model.addAttribute("patients", allPatients);
        return "patients";
    }

    @DeleteMapping("/patients/{id}")
    public String deletePatient(@PathVariable("id") Long patientId) {
        patientService.deletePatient(patientId);
        return "redirect:/patients";
    }
}
