package sda.spring.covid.web.patients;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sda.spring.covid.patients.Patient;
import sda.spring.covid.patients.PatientService;
import sda.spring.covid.patients.PatientValidationException;

import javax.validation.Valid;

@Controller
public class NewPatientController {
    private PatientService patientService;

    public NewPatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping("/patients/new")
    public String showAddPatientForm(Model model) {
        model.addAttribute("patient", new Patient());
        return "patient-new";
    }

    @PostMapping("/patients/new")
    public String addNewPatient(@Valid @ModelAttribute Patient patient, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "patient-new";
        }

        if (patientService.findByPersonalId(patient.getPersonalId()).isPresent()) {
            model.addAttribute("errorMessage", "Person with that id exist");
            return "patient-new";
        }

        try {
            patientService.savePatient(patient);
        } catch (PatientValidationException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "patient-new";
        }

        return "redirect:/patients";
    }
}
