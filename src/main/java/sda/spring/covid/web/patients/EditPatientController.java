package sda.spring.covid.web.patients;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sda.spring.covid.patients.Patient;
import sda.spring.covid.patients.PatientService;
import sda.spring.covid.patients.PatientValidationException;
import sda.spring.covid.security.Roles;
import sda.spring.covid.security.UserService;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class EditPatientController {

    private final PatientService patientService;
    private final UserService userService;

    public EditPatientController(PatientService patientService, UserService userService) {
        this.patientService = patientService;
        this.userService = userService;
    }

    @GetMapping("/patients/edit/{id}")
    public String getEditPatientForm(@PathVariable("id") Long patientId, Model model, RedirectAttributes redirectAttrs,
                                     Authentication authentication) {
        Optional<Patient> patientToEdit = patientService.findById(patientId);

        if (patientToEdit.isPresent()) {
            Patient patient = patientToEdit.get();

            if (!canEditPatient(patient, authentication)) {
                redirectAttrs.addFlashAttribute("errorMessage", "Patient not belong to user");
                return "redirect:/patients";
            }

            model.addAttribute("patient", patientToEdit.get());
            return "patient-edit";
        } else {
            redirectAttrs.addFlashAttribute("errorMessage", "Patient not found");
            return "redirect:/patients";
        }
    }

    private Boolean canEditPatient(Patient patient, Authentication authentication) {
        if (userService.hasRole(Roles.USER.getRoleName())) {
            return patient.getUser() != null && patient.getUser().getLogin().equals(authentication.getName());
        } else {
            return true;
        }
    }

    @PostMapping("/patients/edit/{id}")
    public String updatePatient(@Valid @ModelAttribute("patient") Patient patient, Model model, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "patient-edit";
        }

        try {
            patientService.savePatient(patient);
        } catch (PatientValidationException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "patient-edit";
        }
        return "redirect:/patients";
    }
}
