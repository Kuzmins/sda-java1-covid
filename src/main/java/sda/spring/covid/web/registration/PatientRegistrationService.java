package sda.spring.covid.web.registration;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import sda.spring.covid.patients.Patient;
import sda.spring.covid.patients.PatientService;
import sda.spring.covid.patients.PatientValidationException;
import sda.spring.covid.security.Roles;
import sda.spring.covid.security.User;
import sda.spring.covid.security.UserRegistrationDto;
import sda.spring.covid.security.UserService;

@Component
public class PatientRegistrationService {

    private final UserService userService;
    private final PatientService patientService;

    public PatientRegistrationService(UserService userService, PatientService patientService) {
        this.userService = userService;
        this.patientService = patientService;
    }

    @Transactional
    public void registerPatient(PatientRegistrationDto patientRegistrationDto) throws PatientValidationException {
        User newUser = userService.save(toUserRegistration(patientRegistrationDto), Roles.USER);
        patientService.savePatient(toPatient(patientRegistrationDto, newUser));
    }

    private UserRegistrationDto toUserRegistration(PatientRegistrationDto patientRegistrationDto) {
        UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setLogin(patientRegistrationDto.getEmail());
        userRegistrationDto.setPassword(patientRegistrationDto.getPassword());
        userRegistrationDto.setConfirmPassword(patientRegistrationDto.getConfirmPassword());
        return userRegistrationDto;
    }

    private Patient toPatient(PatientRegistrationDto patientRegistrationDto, User user) {
        Patient patient = new Patient();
        patient.setFirstName(patientRegistrationDto.getFirstName());
        patient.setLastName(patientRegistrationDto.getLastName());
        patient.setPersonalId(patientRegistrationDto.getPersonalId());
        patient.setEmail(patientRegistrationDto.getEmail());
        patient.setUser(user);
        return patient;
    }
}
