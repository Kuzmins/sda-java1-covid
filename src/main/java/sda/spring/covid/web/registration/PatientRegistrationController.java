package sda.spring.covid.web.registration;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import sda.spring.covid.patients.PatientService;
import sda.spring.covid.patients.PatientValidationException;
import sda.spring.covid.security.User;
import sda.spring.covid.security.UserService;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class PatientRegistrationController {
    private final UserService userService;
    private final PatientService patientService;
    private final PatientRegistrationService patientRegistrationService;

    public PatientRegistrationController(UserService userService, PatientService patientService, PatientRegistrationService patientRegistrationService) {
        this.userService = userService;
        this.patientService = patientService;
        this.patientRegistrationService = patientRegistrationService;
    }

    @GetMapping("/registration")
    public String showRegistrationForm(Model model) {
        model.addAttribute("patient", new PatientRegistrationDto());
        return "registration";
    }

    @PostMapping("/registration")
    public String registerUser(@ModelAttribute("patient") @Valid PatientRegistrationDto patientRegistrationDto, BindingResult bindingResult, Model model) {
        Optional<User> existingUser = userService.findByLogin(patientRegistrationDto.getEmail());

        if (existingUser.isPresent()) {
            bindingResult.rejectValue("email", "user_exist", "There is already an user with that email");
        }

        if (patientService.findByPersonalId(patientRegistrationDto.getPersonalId()).isPresent()) {
            bindingResult.rejectValue("personalId", "notUnique", "There is already an user with that personal Id");
        }

        if (bindingResult.hasErrors()) {
            return "registration";
        }


        try {
            patientRegistrationService.registerPatient(patientRegistrationDto);
        } catch (PatientValidationException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "patient-edit";
        }
        return "redirect:/registration?success";
    }
}
