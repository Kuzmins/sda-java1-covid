package sda.spring.covid.web.doctors;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sda.spring.covid.doctor.Doctor;
import sda.spring.covid.doctor.DoctorException;
import sda.spring.covid.doctor.DoctorService;
import sda.spring.covid.validation.ValidationError;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class EditDoctorController {

    private final DoctorService doctorService;

    public EditDoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping("/doctors/edit/{id}")
    public String getEditDoctorForm(@PathVariable("id") Long patientId, Model model, RedirectAttributes redirectAttrs,
                                    Authentication authentication) {
        Optional<Doctor> doctorToEdit = doctorService.findById(patientId);

        if (doctorToEdit.isPresent()) {
            Boolean canEdit = canEdit(doctorToEdit.get(), authentication);
            model.addAttribute("doctor", doctorToEdit.get());
            model.addAttribute("canEdit", canEdit);
            return "doctor-edit";
        } else {
            redirectAttrs.addFlashAttribute("errorMessages", Collections.singletonList("Patient not found"));
            return "redirect:/doctors";
        }
    }

    private Boolean canEdit(Doctor doctor, Authentication authentication) {
        // Write your code here
        return false;
    }

    @PostMapping("/doctors/edit/{id}")
    public String updateDoctor(@Valid @ModelAttribute("doctor") Doctor doctor, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "doctor-edit";
        }

        try {
            doctorService.updateDoctor(doctor);
        } catch (DoctorException ex) {
            List<String> errorMessages = ex.getErrors()
                    .stream()
                    .map(ValidationError::getErrorMessage)
                    .collect(Collectors.toList());

            model.addAttribute("errorMessages", errorMessages);
            return "doctor-edit";
        }

        doctorService.updateDoctor(doctor);
        return "redirect:/doctors";
    }
}
