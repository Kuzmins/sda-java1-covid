package sda.spring.covid.web.doctors;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import sda.spring.covid.doctor.Doctor;
import sda.spring.covid.doctor.DoctorException;
import sda.spring.covid.doctor.DoctorService;
import sda.spring.covid.validation.ValidationError;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class NewDoctorController {
    private final DoctorService doctorService;

    public NewDoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping("/doctors/new")
    public String showAddPatientForm(Model model) {
        model.addAttribute("doctor", new Doctor());
        return "doctor-new";
    }

    @PostMapping("/doctors/new")
    public String addNewPatient(@Valid @ModelAttribute Doctor doctor, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "doctor-new";
        }

        try {
            doctorService.createNewDoctor(doctor);
        } catch (DoctorException ex) {
            List<String> errorMessages = ex.getErrors()
                    .stream()
                    .map(ValidationError::getErrorMessage)
                    .collect(Collectors.toList());

            model.addAttribute("errorMessages", errorMessages);
            return "doctor-new";
        }

        return "redirect:/doctors";
    }
}
