package sda.spring.covid.web.doctors;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import sda.spring.covid.doctor.Doctor;
import sda.spring.covid.doctor.DoctorService;

import java.util.List;

@Controller
public class ListDoctorsController {
    private final DoctorService doctorService;

    public ListDoctorsController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping("/doctors")
    public String showAllDoctors(Model model) {
        List<Doctor> doctors = doctorService.listAllDoctors();
        model.addAttribute("doctors", doctors);
        return "doctors";
    }

    @DeleteMapping("/doctors/{id}")
    public String deleteDoctor(@PathVariable("id") Long doctorId) {
        doctorService.deleteDoctor(doctorId);
        return "redirect:/doctors";
    }
}
