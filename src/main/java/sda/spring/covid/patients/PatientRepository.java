package sda.spring.covid.patients;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PatientRepository extends JpaRepository<Patient, Long> {
    Patient findByFirstName(String firstName);
    List<Patient> findByLastName(String lastName);
    Patient findByEmail(String email);
    Optional<Patient> findByPersonalId(String personalId);
    List<Patient> findAllByUserId(Long userId);
    Optional<Patient> findByUserLogin(String login);
}
