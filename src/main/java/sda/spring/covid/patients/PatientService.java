package sda.spring.covid.patients;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import sda.spring.covid.blacklist.BlackList;
import sda.spring.covid.blacklist.BlackListService;
import sda.spring.covid.security.Roles;
import sda.spring.covid.security.User;
import sda.spring.covid.security.UserService;

import java.util.List;
import java.util.Optional;

@Component
public class PatientService {

    private final PatientRepository patientRepository;

    private final UserService userService;

    private final BlackListService blackListService;

    public PatientService(PatientRepository patientRepository, UserService userService, BlackListService blackListService) {
        this.patientRepository = patientRepository;
        this.userService = userService;
        this.blackListService = blackListService;
    }

    @Transactional(readOnly = true)
    public List<Patient> listAllPatients() {
        if (userService.hasRole(Roles.USER.getRoleName())) {
            User user = userService.getCurrentLoggedInUser();
            return patientRepository.findAllByUserId(user.getId());
        }

        return patientRepository.findAll();
    }


    @Transactional(readOnly = true)
    public Optional<Patient> findById(Long patientId) {
        return patientRepository.findById(patientId);
    }

    @Transactional(readOnly = true)
    public Optional<Patient> findByPersonalId(String personalId) {
        return patientRepository.findByPersonalId(personalId);
    }

    @Transactional
    public void savePatient(Patient patient) throws PatientValidationException {
        Optional<BlackList> probableBlackList = blackListService.findByPersonalId(patient.getPersonalId());
        if (probableBlackList.isPresent()) {
            throw new PatientValidationException("Patient in black list. Reason: " + probableBlackList.get().getReason());
        }
        patientRepository.save(patient);
    }

    @Transactional
    public void deletePatient(Long patientId) {
        patientRepository.deleteById(patientId);
    }
}
