package sda.spring.covid.patients;

public class PatientValidationException extends Exception {

    public PatientValidationException(String message) {
        super(message);
    }
}
