package sda.spring.covid.rest.blacklists;

import org.springframework.stereotype.Component;
import sda.spring.covid.blacklist.BlackList;

@Component
public class BlackListMapper {

    public BlackListDto toDto(BlackList blackList) {
        BlackListDto blackListDto = new BlackListDto();
        blackListDto.setId(blackList.getId());
        blackListDto.setPersonalId(blackList.getPersonalId());
        blackListDto.setReason(blackList.getReason());
        return blackListDto;
    }

    public BlackList toEntity(BlackListDto blackListDto) {
        BlackList blackList = new BlackList();
        blackList.setId(blackListDto.getId());
        blackList.setPersonalId(blackListDto.getPersonalId());
        blackList.setReason(blackListDto.getReason());
        return blackList;
    }
}
