package sda.spring.covid.rest.blacklists;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import sda.spring.covid.blacklist.BlackList;
import sda.spring.covid.blacklist.BlackListService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class BlackListRestController {

    private final BlackListService blackListService;
    private final BlackListMapper blackListMapper;

    public BlackListRestController(BlackListService blackListService, BlackListMapper blackListMapper) {
        this.blackListService = blackListService;
        this.blackListMapper = blackListMapper;
    }

    @GetMapping("/api/blacklists")
    @Secured("ROLE_ADMIN")
    public List<BlackListDto> getAllBlackLists() {
        List<BlackList> allBlacklistEntries = blackListService.listAllInBlackList();
        return allBlacklistEntries.stream().map(blackListMapper::toDto).collect(Collectors.toList());
    }

    @GetMapping("/api/blacklists/{id}")
    @Secured("ROLE_ADMIN")
    public BlackListDto getBlackListEntry(@PathVariable("id") Long blackListId) {
        BlackList blackList = blackListService.findById(blackListId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Blacklist entry not found"));
        return blackListMapper.toDto(blackList);
    }

    @PostMapping("/api/blacklists")
    @Secured("ROLE_ADMIN")
    public BlackListDto createNewBlackListEntry(@RequestBody @Valid BlackListDto blackListDto) {
        BlackList blackList = blackListMapper.toEntity(blackListDto);
        blackListService.saveBlackList(blackList);
        return blackListMapper.toDto(blackList);
    }

    @PutMapping("/api/blacklists/{id}")
    @Secured("ROLE_ADMIN")
    public BlackListDto updateBlackListEntry(@RequestBody @Valid BlackListDto blackListDto, @PathVariable("id") Long blackListId) {
        blackListService.findById(blackListId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Blacklist entry not found"));

        BlackList blackList = blackListMapper.toEntity(blackListDto);
        blackListService.saveBlackList(blackList);
        return blackListMapper.toDto(blackList);
    }

    @DeleteMapping("/api/blacklists/{id}")
    @Secured("ROLE_ADMIN")
    public void deleteBlackListEntry( @PathVariable("id") Long blackListId) {
        blackListService.deleteFromBlackList(blackListId);
    }}
