package sda.spring.covid.rest.blacklists;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class BlackListDto {

    private Long id;

    @Pattern(regexp = "^(\\d{6})-[012]\\d{4}$", message = "Not valid personalId!")
    private String personalId;

    @NotBlank
    private String reason;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
