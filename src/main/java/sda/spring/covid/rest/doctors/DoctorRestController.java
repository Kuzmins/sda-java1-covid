package sda.spring.covid.rest.doctors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import sda.spring.covid.doctor.Doctor;
import sda.spring.covid.doctor.DoctorService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class DoctorRestController {

    private final DoctorService doctorService;

    private final DoctorMapper doctorMapper;

    public DoctorRestController(DoctorService doctorService, DoctorMapper doctorMapper) {
        this.doctorService = doctorService;
        this.doctorMapper = doctorMapper;
    }

    @GetMapping("/api/doctors")
    public List<DoctorDto> getAllDoctors() {
        List<Doctor> allDoctors = doctorService.listAllDoctors();
        return allDoctors.stream().map(doctorMapper::toDto).collect(Collectors.toList());
    }

    @GetMapping("/api/doctors/{id}")
    public DoctorDto getDoctor(@PathVariable("id") Long doctorId) {
        Doctor doctor = doctorService.findById(doctorId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Doctor not found"));
        return doctorMapper.toDto(doctor);
    }

    @PostMapping("/api/doctors")
    public DoctorDto createNewDoctor(@RequestBody @Valid DoctorDto doctorDto) {
        Doctor doctor = doctorMapper.toEntity(doctorDto);
        doctorService.createNewDoctor(doctor);
        return doctorMapper.toDto(doctor);
    }

    @PutMapping("/api/doctors/{id}")
    public DoctorDto updateDoctor(@RequestBody @Valid DoctorDto doctorDto, @PathVariable("id") Long doctorId) {
        doctorService.findById(doctorId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Doctor not found"));

        Doctor doctor = doctorMapper.toEntity(doctorDto);
        doctorService.updateDoctor(doctor);
        return doctorMapper.toDto(doctor);
    }

    @DeleteMapping("/api/doctors/{id}")
    public void deleteDoctor( @PathVariable("id") Long doctorId) {
        doctorService.deleteDoctor(doctorId);
    }
}
