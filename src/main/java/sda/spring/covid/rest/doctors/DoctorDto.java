package sda.spring.covid.rest.doctors;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class DoctorDto {
    private Long id;

    @Size(min = 2, max = 30)
    private String firstName;

    @Size(min = 2, max = 30)
    private String lastName;

    @Pattern(regexp = "^(\\d{6})-[012]\\d{4}$", message = "Not valid personalId!")
    private String personalId;

    @Size(min = 2, max = 30)
    private String clinicName;

    @Size(min = 2, max = 30)
    private String licenceId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public String getClinicName() {
        return clinicName;
    }

    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }

    public String getLicenceId() {
        return licenceId;
    }

    public void setLicenceId(String licenceId) {
        this.licenceId = licenceId;
    }
}
