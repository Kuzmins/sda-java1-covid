package sda.spring.covid.rest.doctors;

import org.springframework.stereotype.Component;
import sda.spring.covid.doctor.Doctor;

@Component
public class DoctorMapper {

    public DoctorDto toDto(Doctor doctor) {
        DoctorDto doctorDto = new DoctorDto();
        doctorDto.setId(doctor.getId());
        doctorDto.setFirstName(doctor.getFirstName());
        doctorDto.setLastName(doctor.getLastName());
        doctorDto.setPersonalId(doctor.getPersonalId());
        doctorDto.setLicenceId(doctor.getLicenceId());
        doctorDto.setClinicName(doctor.getClinicName());
        return doctorDto;
    }

    public Doctor toEntity(DoctorDto doctorDto) {
        Doctor doctor = new Doctor();
        doctor.setId(doctorDto.getId());
        doctor.setFirstName(doctorDto.getFirstName());
        doctor.setLastName(doctorDto.getLastName());
        doctor.setPersonalId(doctorDto.getPersonalId());
        doctor.setLicenceId(doctorDto.getLicenceId());
        doctor.setClinicName(doctorDto.getClinicName());
        return doctor;
    }
}
