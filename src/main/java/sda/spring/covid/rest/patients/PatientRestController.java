package sda.spring.covid.rest.patients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import sda.spring.covid.patients.Patient;
import sda.spring.covid.patients.PatientService;
import sda.spring.covid.patients.PatientValidationException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PatientRestController {

    private final PatientMapper patientMapper;
    private final PatientService patientService;

    public PatientRestController(PatientMapper patientMapper, PatientService patientService) {
        this.patientMapper = patientMapper;
        this.patientService = patientService;
    }

    @GetMapping("/api/patients")
    public List<PatientDto> getAllPatients() {
        List<Patient> allPatients = patientService.listAllPatients();
        return allPatients.stream().map(patientMapper::toDto).collect(Collectors.toList());
    }

    @GetMapping("/api/patients/{id}")
    public PatientDto getPatient(@PathVariable("id") Long patientId) {
        Patient patient = patientService.findById(patientId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Patient not found"));
        return patientMapper.toDto(patient);
    }

    @PostMapping("/api/patients")
    public PatientDto createNewPatient(@RequestBody @Valid PatientDto patientDto) {
        Patient patient = patientMapper.toEntity(patientDto);
        savePatient(patient);
        return patientMapper.toDto(patient);
    }

    @PutMapping("/api/patients/{id}")
    public PatientDto updatePatient(@RequestBody @Valid PatientDto patientDto, @PathVariable("id") Long patientId) {
        Patient patientToEdit = patientService.findById(patientId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Patient not found"));

        patientToEdit.setPersonalId(patientDto.getPersonalId());
        patientToEdit.setFirstName(patientDto.getFirstName());
        patientToEdit.setLastName(patientDto.getLastName());
        patientToEdit.setPersonalId(patientDto.getPersonalId());
        patientToEdit.setEmail(patientDto.getEmail());
        savePatient(patientToEdit);
        return patientMapper.toDto(patientToEdit);
    }

    private void savePatient(Patient patient) {
        try {
            patientService.savePatient(patient);
        } catch (PatientValidationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/api/patients/{id}")
    public void deletePatient( @PathVariable("id") Long patientId) {
        patientService.deletePatient(patientId);
    }
}
