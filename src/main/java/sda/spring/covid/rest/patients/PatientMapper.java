package sda.spring.covid.rest.patients;

import org.springframework.stereotype.Component;
import sda.spring.covid.patients.Patient;

@Component
public class PatientMapper {
    public PatientDto toDto(Patient patient) {
        PatientDto patientDto = new PatientDto();
        patientDto.setFirstName(patient.getFirstName());
        patientDto.setLastName(patient.getLastName());
        patientDto.setEmail(patient.getEmail());
        patientDto.setPersonalId(patient.getPersonalId());
        patientDto.setId(patient.getId());
        return patientDto;
    }

    public Patient toEntity(PatientDto patientDto) {
        Patient patient = new Patient();
        patient.setEmail(patientDto.getEmail());
        patient.setFirstName(patientDto.getFirstName());
        patient.setLastName(patientDto.getLastName());
        patient.setPersonalId(patientDto.getPersonalId());
        patient.setId(patientDto.getId());
        return patient;
    }
}
