package sda.spring.covid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import sda.spring.covid.blacklist.BlackList;
import sda.spring.covid.blacklist.BlackListRepository;
import sda.spring.covid.doctor.Doctor;
import sda.spring.covid.doctor.DoctorRepository;
import sda.spring.covid.patients.Patient;
import sda.spring.covid.patients.PatientRepository;
import sda.spring.covid.security.*;

import java.util.List;

@Component
public class CovidCommandLineRunner implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(CovidCommandLineRunner.class);
    private final DoctorRepository doctorRepository;

    private PatientRepository patientRepository;

    private final BlackListRepository blackListRepository;

    private final RoleRepository roleRepository;

    private final UserService userService;

    public CovidCommandLineRunner(DoctorRepository doctorRepository, PatientRepository patientRepository, BlackListRepository blackListRepository, RoleRepository roleRepository, UserService userService) {
        this.doctorRepository = doctorRepository;
        this.patientRepository = patientRepository;
        this.blackListRepository = blackListRepository;
        this.roleRepository = roleRepository;
        this.userService = userService;
    }

    /*

     Tasks:
     1) Create patient and save it
     2) Find all patients by name.
     3) Find all patients by lastName.
     4) Find patient by email.
     5) Delete patient by id.

     6) Create doctor repository and inject it here.
     7) Save doctor.
     8) Find doctor by licenceId.
     9) Find all doctors by name.

     10) Create blacklist repository and inject it.
     11) Create black list record.
     12) List all records from blacklist.
     */
    @Override
    @Transactional
    public void run(String... args) throws Exception {
        log.info("I am alive");

        Patient patient2 = new Patient();
        patient2.setFirstName("Janis");
        patient2.setLastName("Ozols");
        patient2.setEmail("ozols@inbox.lv");
        patient2.setPersonalId("123456-12347");

        patientRepository.save(patient2);

        Doctor doctor = new Doctor();
        doctor.setFirstName("Janis");
        doctor.setLastName("Ozols");
        doctor.setPersonalId("123456-12347");
        doctor.setClinicName("aaa332sss");
        doctor.setLicenceId("2211122");

        doctorRepository.save(doctor);


        BlackList blackList = new BlackList();
        blackList.setPersonalId("123456-12347");
        blackList.setReason("aaa332sss");


        blackListRepository.save(blackList);

        roleRepository.save(new Role(Roles.DOCTOR));
        roleRepository.save(new Role(Roles.ADMIN));
        roleRepository.save(new Role(Roles.USER));

        UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        userRegistrationDto.setLogin("doctor");
        userRegistrationDto.setPassword("doctor");
        userRegistrationDto.setConfirmPassword("doctor");
        userService.save(userRegistrationDto, Roles.DOCTOR);


        userRegistrationDto.setLogin("user");
        userRegistrationDto.setPassword("user");
        userRegistrationDto.setConfirmPassword("user");
        User user = userService.save(userRegistrationDto, Roles.USER);

        Patient patient = new Patient();
        patient.setFirstName("John");
        patient.setLastName("Ivanov");
        patient.setEmail("ivanov@inbox.lv");
        patient.setPersonalId("123456-12345");
        patient.setUser(user);

        patientRepository.save(patient);

        userRegistrationDto.setLogin("admin");
        userRegistrationDto.setPassword("admin");
        userRegistrationDto.setConfirmPassword("admin");
        userService.save(userRegistrationDto, Roles.ADMIN);
    }

}

