package sda.spring.covid.blacklist;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;

@Component
public class BlackListService {


    private final BlackListRepository blackListRepository;

    public BlackListService(BlackListRepository blackListRepository) {
        this.blackListRepository = blackListRepository;
    }

    @Transactional(readOnly = true)
    public List<BlackList> listAllInBlackList() {
        return blackListRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<BlackList> findById(Long patientId) {
        return blackListRepository.findById(patientId);
    }

    @Transactional(readOnly = true)
    public Optional<BlackList> findByPersonalId(String personalId) {
        return blackListRepository.findByPersonalId(personalId);
    }

    @Transactional
    public void saveBlackList(BlackList blackList) {
        blackListRepository.save(blackList);
    }

    @Transactional
    public void deleteFromBlackList(Long blackListId) {
        blackListRepository.deleteById(blackListId);
    }
}