package sda.spring.covid.blacklist;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

/*
* Create Black list entity with id, personalId, reason.
* */
@Entity
@Table(name = "blacklist")
public class BlackList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Pattern(regexp = "^(\\d{6})-[012]\\d{4}$", message = "Not valid personalId!")
    @Column(name = "personal_Id")
    private String personalId;

    @Column(name = "reason")
    private String reason;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "BlackList{" +
                "id=" + id +
                ", personalId='" + personalId + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
