package sda.spring.covid.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final DatabaseUserDetailsService userDetailsService;

    public SecurityConfiguration(DatabaseUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Configuration
    @Order(1)
    public class ApiSecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .antMatcher("/api/**")
                    .csrf().disable()
                    .authorizeRequests()
                    .antMatchers(HttpMethod.POST, "/api/patients/**").hasAnyRole(Roles.DOCTOR.name(), Roles.ADMIN.name())
                    .antMatchers(HttpMethod.DELETE, "/api/patients/**").hasAnyRole(Roles.ADMIN.name())
                    .antMatchers(HttpMethod.POST,"/api/doctors/**").hasAnyRole(Roles.ADMIN.name())
                    .antMatchers(HttpMethod.GET, "/api/doctors/**").hasAnyRole(Roles.DOCTOR.name(), Roles.ADMIN.name())
                    .antMatchers(HttpMethod.DELETE,"/api/doctors/**").hasAnyRole(Roles.ADMIN.name())
                    .anyRequest().authenticated()
                    .and()
                    .httpBasic()
                    .and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        }
    }

    @Configuration
    @Order(2)
    public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .antMatcher("/**").csrf()
                    .and()
                    .authorizeRequests()
                    .antMatchers("/patients/new/**").hasAnyRole(Roles.DOCTOR.name(), Roles.ADMIN.name())
                    .antMatchers(HttpMethod.POST, "/patients").hasAnyRole(Roles.DOCTOR.name(), Roles.ADMIN.name())
                    .antMatchers("/doctors/**").hasAnyRole(Roles.DOCTOR.name(), Roles.ADMIN.name())
                    .antMatchers("/blacklist").hasRole(Roles.ADMIN.name())
                    .antMatchers("/registration/**").permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .formLogin()
                    .loginPage("/login")
                    .permitAll()
                    .and()
                    .logout()
                    .invalidateHttpSession(true)
                    .clearAuthentication(true)
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                    .logoutSuccessUrl("/login?logout");
        }
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }
}
