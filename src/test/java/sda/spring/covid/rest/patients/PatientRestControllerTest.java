package sda.spring.covid.rest.patients;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import sda.spring.covid.blacklist.BlackList;
import sda.spring.covid.blacklist.BlackListService;
import sda.spring.covid.rest.BaseRestTest;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class PatientRestControllerTest extends BaseRestTest {

    @Autowired
    private BlackListService blackListService;

    @Test
    @WithMockUser(roles = "ADMIN")
    public void testPatientCrud() throws Exception {

        // Create new patient
        PatientDto patientDto = new PatientDto();
        patientDto.setFirstName("Test");
        patientDto.setLastName("Test2");
        patientDto.setEmail("test@test.com");
        patientDto.setPersonalId("123456-11345");

        String patientJson = mockMvc.perform(MockMvcRequestBuilders.post("/api/patients")
                .content(mapper.writeValueAsString(patientDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PatientDto createdPatient = mapper.readValue(patientJson, PatientDto.class);


        // Check patients size increased
        mockMvc.perform(MockMvcRequestBuilders.get("/api/patients"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)));


        // Delete patient
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/patients/" + createdPatient.getId()))
                .andDo(print())
                .andExpect(status().isOk());

        // Check patients size decreased
        mockMvc.perform(MockMvcRequestBuilders.get("/api/patients"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)));
    }


    @Test
    @WithMockUser(roles = "ADMIN")
    public void testPatientInBlackListOnCreate() throws Exception {

        BlackList testBlackListEntry = new BlackList();
        testBlackListEntry.setPersonalId("876123-16537");
        testBlackListEntry.setReason("For testing.");

        blackListService.saveBlackList(testBlackListEntry);

        // Create new patient
        PatientDto patientDto = new PatientDto();
        patientDto.setFirstName("Test");
        patientDto.setLastName("Test2");
        patientDto.setEmail("test@test.com");
        patientDto.setPersonalId(testBlackListEntry.getPersonalId());

        // Check patient can not be created
        // String expectedError = "Patient in black list. Reason: For testing.";

        mockMvc.perform(MockMvcRequestBuilders.post("/api/patients")
                .content(mapper.writeValueAsString(patientDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void testPatientInBlackListOnUpdate() throws Exception {
        BlackList testBlackListEntry = new BlackList();
        testBlackListEntry.setPersonalId("876122-16537");
        testBlackListEntry.setReason("For testing.");

        blackListService.saveBlackList(testBlackListEntry);

        // Update Patient
        PatientDto patientDto = new PatientDto();
        patientDto.setFirstName("Test");
        patientDto.setLastName("Test2");
        patientDto.setEmail("test@test.com");
        patientDto.setPersonalId(testBlackListEntry.getPersonalId());

        // Check patient can not be created
        // String expectedError = "Patient in black list. Reason: For testing.";

        mockMvc.perform(MockMvcRequestBuilders.put("/api/patients/1")
                .content(mapper.writeValueAsString(patientDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }




}