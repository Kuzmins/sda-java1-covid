package sda.spring.covid.rest.blacklists;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import sda.spring.covid.rest.BaseRestTest;
import sda.spring.covid.rest.doctors.DoctorDto;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class BlackListRestControllerTest extends BaseRestTest {

    @Test
    @WithMockUser(roles = "ADMIN")
    public void testBlackListFullCrud() throws Exception {

        // Create blackList entry
        BlackListDto blackListDto = new BlackListDto();
        blackListDto.setPersonalId("111111-11111");
        blackListDto.setReason("Testing");

        String blackListJson = mockMvc.perform(MockMvcRequestBuilders.post("/api/blacklists")
                .content(mapper.writeValueAsString(blackListDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        BlackListDto createdBlackList = mapper.readValue(blackListJson, BlackListDto.class);

        // Update Black list
        createdBlackList.setReason("Updated reason");

        mockMvc.perform(MockMvcRequestBuilders.put("/api/blacklists/" + createdBlackList.getId())
                .content(mapper.writeValueAsString(createdBlackList))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // Read and check black list entry updated
        mockMvc.perform(MockMvcRequestBuilders.get("/api/blacklists/" + createdBlackList.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reason").value("Updated reason"));

        // Delete black list entry
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/blacklists/" + createdBlackList.getId()))
                .andDo(print())
                .andExpect(status().isOk());

        // Check black list entries size same as before
        mockMvc.perform(MockMvcRequestBuilders.get("/api/doctors"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)));
    }
}