package sda.spring.covid.rest.doctors;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import sda.spring.covid.rest.BaseRestTest;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class DoctorRestControllerTest extends BaseRestTest {

    @Test
    @WithMockUser(roles = "ADMIN")
    public void shouldGetAllDoctors() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/doctors"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void testDoctorFullCrud() throws Exception {

        // Create new doctor
        DoctorDto doctor = new DoctorDto();
        doctor.setFirstName("Ivan");
        doctor.setLastName("Ivanovs");
        doctor.setPersonalId("111111-22222");
        doctor.setClinicName("Super awesome clinic");
        doctor.setLicenceId("11112233221");

        String doctorJson = mockMvc.perform(MockMvcRequestBuilders.post("/api/doctors")
                .content(mapper.writeValueAsString(doctor))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        DoctorDto createdDoctor = mapper.readValue(doctorJson, DoctorDto.class);

        // Update Doctor
        createdDoctor.setFirstName("New name");

        mockMvc.perform(MockMvcRequestBuilders.put("/api/doctors/" + createdDoctor.getId())
                .content(mapper.writeValueAsString(createdDoctor))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // Read check doctor updated
        mockMvc.perform(MockMvcRequestBuilders.get("/api/doctors/" + createdDoctor.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("New name"));

        // Delete doctor
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/doctors/" + createdDoctor.getId()))
                .andDo(print())
                .andExpect(status().isOk());

        // Check doctors size same as before
        mockMvc.perform(MockMvcRequestBuilders.get("/api/doctors"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)));
    }

}