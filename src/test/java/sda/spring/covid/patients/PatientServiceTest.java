package sda.spring.covid.patients;

import org.junit.jupiter.api.Test;
import sda.spring.covid.blacklist.BlackList;
import sda.spring.covid.blacklist.BlackListService;
import sda.spring.covid.security.UserService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class PatientServiceTest {

    private BlackListService blackListService = mock(BlackListService.class);
    private UserService userService = mock(UserService.class);
    private PatientRepository patientRepository = mock(PatientRepository.class);

    private PatientService patientService = new PatientService(patientRepository, userService, blackListService);

    @Test
    void shouldShowAllPatientsIfNotUserRole() {

    }

    @Test
    void shouldShowOnlyCurrentUser() {

    }

    @Test
    void shouldNotSaveUserIfInBlackList() {

    }

    @Test
    void shouldSaveUserIfNotInBlackList() {

    }
}